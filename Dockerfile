FROM maven:3.6.3-openjdk-17-slim AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:17-alpine AS app
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=build /usr/src/app/target/hackathon-0.0.1-SNAPSHOT.jar /usr/app/hackathon-0.0.1-SNAPSHOT.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/usr/app/hackathon-0.0.1-SNAPSHOT.jar"]

#docker build -t hackathon-backend-image:latest .

# docker run -d -p 8081:8081 hackathon-backend-image:latest