package com.hackathon.hackathon.services;

import com.hackathon.hackathon.models.FilmModel;
import com.hackathon.hackathon.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FilmService {

    @Autowired
    FilmRepository filmRepository;

    public List<FilmModel> findAll() {
        System.out.println("findAll en FilmService");

        return this.filmRepository.findAll();
    }

    public FilmModel add(FilmModel product) {
        System.out.println("add en FilmService");

        return this.filmRepository.save(product);
    }

    public Optional<FilmModel> findById(String id){
        System.out.println("findById en FilmService");

        return this.filmRepository.findById(id);
    }

    public boolean delete(String id) {
        System.out.println("delete en FilmModel");
        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("Film encontrada, borrando");

            this.filmRepository.deleteById(id);
            result = true;
        }

        return result;
    }

    public FilmModel update(FilmModel filmModel) {
        System.out.println("update en FilmModel");

        return this.filmRepository.save(filmModel);
    }
}
