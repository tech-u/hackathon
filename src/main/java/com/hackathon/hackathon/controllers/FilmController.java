package com.hackathon.hackathon.controllers;

import com.hackathon.hackathon.models.FilmModel;
import com.hackathon.hackathon.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class FilmController {

    @Autowired
    FilmService filmService;

    @RequestMapping("/films")
    public ResponseEntity<List<FilmModel>> getFilms(){
        System.out.println("getFilms");

        return new ResponseEntity<>(
                this.filmService.findAll(),
                HttpStatus.OK
        );
    }

    @PostMapping("/films")
    public ResponseEntity<FilmModel> addFilm(@RequestBody FilmModel film){
        System.out.println("addFilm");
        System.out.println("La id de la film a crear es: " + film.getId());

        return new ResponseEntity<>(
                this.filmService.add(film),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/films/{id}")
    public ResponseEntity getFilmById(@PathVariable String id){
        System.out.println("getFilmById");
        System.out.println("La id de la film a buscar es: " + id);

        Optional<FilmModel> result = filmService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Film no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/films/{id}")
    public ResponseEntity<String> deleteFilm(@PathVariable String id) {
        System.out.println("deleteFilm");
        System.out.println("La id de la film a borrar es:" + id);

        boolean deleteFilm = this.filmService.delete(id);

        return new ResponseEntity<>(
                deleteFilm ? "Film borrada" : "Film no encontrada",
                deleteFilm ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/films/{id}")
    public ResponseEntity<FilmModel> updateFilm(@RequestBody FilmModel filmModel, @PathVariable String id){
        System.out.println("updateFilm");

        Optional<FilmModel> filmToUpdate = this.filmService.findById(id);

        if(filmToUpdate.isPresent()){
            System.out.println("Film para actualizar encontrado, actualizando");

            this.filmService.update(filmModel);
        }

        return new ResponseEntity<>(
                filmModel, filmToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
