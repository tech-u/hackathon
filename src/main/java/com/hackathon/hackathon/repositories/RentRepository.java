package com.hackathon.hackathon.repositories;

import com.hackathon.hackathon.models.RentModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RentRepository extends MongoRepository<RentModel, String> {
}
