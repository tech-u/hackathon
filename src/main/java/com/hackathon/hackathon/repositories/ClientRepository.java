package com.hackathon.hackathon.repositories;

import com.hackathon.hackathon.models.ClientModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends MongoRepository<ClientModel, String> {
}
